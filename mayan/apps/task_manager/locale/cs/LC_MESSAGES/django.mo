��          �      |      �     �     �  	        "     9     H     M     [     m     s     x     �     �     �  
   �     �  
   �     �      �  
        $  �  6             	   /     9     S     e     n     ~  
   �     �     �  "   �     �  #   �          +     :     L  !   P     r     �                                                 
                            	                    Active tasks Active tasks in queue: %s Arguments Background task queues Default queue? Host Is transient? Keyword arguments Label Name Reserved tasks Reserved tasks in queue: %s Scheduled tasks Scheduled tasks in queue: %s Start time Task manager Test queue Type Unable to retrieve task list; %s View tasks Worker process ID Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-06-30 22:04+0000
Last-Translator: Michal Švábík <snadno@lehce.cz>, 2019
Language-Team: Czech (https://www.transifex.com/rosarior/teams/13584/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 Aktivní úkoly Aktivní úkoly ve frontě: %s Argumenty Fronty úkolů na pozadí Výchozí fronta? Hostitel Je přechodný? Argumenty klíčových slov Označení název Vyhrazené úkoly Rezervované úkoly ve frontě: %s Naplánované úkoly Naplánované úlohy ve frontě: %s Doba spuštění Správce úloh Testovací fronta Typ Nelze načíst seznam úkolů; %s Zobrazit úkoly ID pracovního procesu 